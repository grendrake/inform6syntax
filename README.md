# inform6 README

This is a basic syntax highlighter for Inform 6 source code.

## Release Notes

Users appreciate release notes as you update your extension.

### 1.0.0

Initial release of Inform 6 Syntax Highlighter.
